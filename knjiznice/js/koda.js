
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}

var imeIndeks;
var priimekIndeks;
var datumInUraIndeks;
var dodatneInformacije;
var tezaIndeks;
var visinaIndeks;
var datumIndeks;

/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
 var pacientiVzorec = [
    ["Jože", "Robidar", "1939-03-15T09:03", "1"],
    ["Katarina", "Barbara", "1996-04-29T18:45", "0"],
    ["Franc", "Smrekar", "1965-07-02T21:14", "2"],
    ["", "", "", "", "", "", ""]];
    
var meritveVzorec = [
    ["9ab860f0-0f48-4259-9cf1-1288a6649fa8", "2015-08-13T07:54", "182", "87"],
    ["4b9efbfd-5335-47c1-a578-215946f85a39", "2016-07-18T09:34", "166", "59"],
    ["629bc01f-2c5e-4be5-b13a-25b92d16bb0c", "2016-09-02T11:04", "175", "105"],
    ["", "", "", "", "", "", "", ""]];
    
var drzave = 
[
 {
   "FIELD1": "Slovenija",
   "FIELD2": "SVN"
 },
 {
   "FIELD1": "Afghanistan",
   "FIELD2": "AFG"
 },
 {
   "FIELD1": "Albania",
   "FIELD2": "ALB"
 },
 {
   "FIELD1": "Algeria",
   "FIELD2": "DZA"
 },
 {
   "FIELD1": "Angola",
   "FIELD2": "AGO"
 },
 {
   "FIELD1": "Antarctica",
   "FIELD2": "ATA"
 },
 {
   "FIELD1": "Argentina",
   "FIELD2": "ARG"
 },
 {
   "FIELD1": "Armenia",
   "FIELD2": "ARM"
 },
 {
   "FIELD1": "Australia",
   "FIELD2": "AUS"
 },
 {
   "FIELD1": "Austria",
   "FIELD2": "AUT"
 },
 {
   "FIELD1": "Azerbaijan",
   "FIELD2": "AZE"
 },
 {
   "FIELD1": "Bahamas",
   "FIELD2": "BHS"
 },
 {
   "FIELD1": "Bahrain",
   "FIELD2": "BHR"
 },
 {
   "FIELD1": "Bangladesh",
   "FIELD2": "BGD"
 },
 {
   "FIELD1": "Barbados",
   "FIELD2": "BRB"
 },
 {
   "FIELD1": "Belarus",
   "FIELD2": "BLR"
 },
 {
   "FIELD1": "Belgium",
   "FIELD2": "BEL"
 },
 {
   "FIELD1": "Belize",
   "FIELD2": "BLZ"
 },
 {
   "FIELD1": "Benin",
   "FIELD2": "BEN"
 },
 {
   "FIELD1": "Bhutan",
   "FIELD2": "BTN"
 },
 {
   "FIELD1": "Bolivia (Plurinational State of)",
   "FIELD2": "BOL"
 },
 {
   "FIELD1": "Bosnia and Herzegovina",
   "FIELD2": "BIH"
 },
 {
   "FIELD1": "Botswana",
   "FIELD2": "BWA"
 },
 {
   "FIELD1": "Brazil",
   "FIELD2": "BRA"
 },
 {
   "FIELD1": "Brunei Darussalam",
   "FIELD2": "BRN"
 },
 {
   "FIELD1": "Bulgaria",
   "FIELD2": "BGR"
 },
 {
   "FIELD1": "Burkina Faso",
   "FIELD2": "BFA"
 },
 {
   "FIELD1": "Burundi",
   "FIELD2": "BDI"
 },
 {
   "FIELD1": "Cabo Verde",
   "FIELD2": "CPV"
 },
 {
   "FIELD1": "Cambodia",
   "FIELD2": "KHM"
 },
 {
   "FIELD1": "Cameroon",
   "FIELD2": "CMR"
 },
 {
   "FIELD1": "Canada",
   "FIELD2": "CAN"
 },
 {
   "FIELD1": "Central African Republic",
   "FIELD2": "CAF"
 },
 {
   "FIELD1": "Chad",
   "FIELD2": "TCD"
 },
 {
   "FIELD1": "Chile",
   "FIELD2": "CHL"
 },
 {
   "FIELD1": "China",
   "FIELD2": "CHN"
 },
 {
   "FIELD1": "Colombia",
   "FIELD2": "COL"
 },
 {
   "FIELD1": "Comoros",
   "FIELD2": "COM"
 },
 {
   "FIELD1": "Congo",
   "FIELD2": "COG"
 },
 {
   "FIELD1": "Congo (Democratic Republic of the)",
   "FIELD2": "COD"
 },
 {
   "FIELD1": "Costa Rica",
   "FIELD2": "CRI"
 },
 {
   "FIELD1": "Côte d'Ivoire",
   "FIELD2": "CIV"
 },
 {
   "FIELD1": "Croatia",
   "FIELD2": "HRV"
 },
 {
   "FIELD1": "Cuba",
   "FIELD2": "CUB"
 },
 {
   "FIELD1": "Cyprus",
   "FIELD2": "CYP"
 },
 {
   "FIELD1": "Czechia",
   "FIELD2": "CZE"
 },
 {
   "FIELD1": "Denmark",
   "FIELD2": "DNK"
 },
 {
   "FIELD1": "Djibouti",
   "FIELD2": "DJI"
 },
 {
   "FIELD1": "Dominica",
   "FIELD2": "DMA"
 },
 {
   "FIELD1": "Dominican Republic",
   "FIELD2": "DOM"
 },
 {
   "FIELD1": "Ecuador",
   "FIELD2": "ECU"
 },
 {
   "FIELD1": "Egypt",
   "FIELD2": "EGY"
 },
 {
   "FIELD1": "El Salvador",
   "FIELD2": "SLV"
 },
 {
   "FIELD1": "Equatorial Guinea",
   "FIELD2": "GNQ"
 },
 {
   "FIELD1": "Eritrea",
   "FIELD2": "ERI"
 },
 {
   "FIELD1": "Estonia",
   "FIELD2": "EST"
 },
 {
   "FIELD1": "Ethiopia",
   "FIELD2": "ETH"
 },
 {
   "FIELD1": "Fiji",
   "FIELD2": "FJI"
 },
 {
   "FIELD1": "Finland",
   "FIELD2": "FIN"
 },
 {
   "FIELD1": "France",
   "FIELD2": "FRA"
 },
 {
   "FIELD1": "Gabon",
   "FIELD2": "GAB"
 },
 {
   "FIELD1": "Gambia",
   "FIELD2": "GMB"
 },
 {
   "FIELD1": "Georgia",
   "FIELD2": "GEO"
 },
 {
   "FIELD1": "Germany",
   "FIELD2": "DEU"
 },
 {
   "FIELD1": "Ghana",
   "FIELD2": "GHA"
 },
 {
   "FIELD1": "Greece",
   "FIELD2": "GRC"
 },
 {
   "FIELD1": "Grenada",
   "FIELD2": "GRD"
 },
 {
   "FIELD1": "Guatemala",
   "FIELD2": "GTM"
 },
 {
   "FIELD1": "Guinea",
   "FIELD2": "GIN"
 },
 {
   "FIELD1": "Guinea-Bissau",
   "FIELD2": "GNB"
 },
 {
   "FIELD1": "Guyana",
   "FIELD2": "GUY"
 },
 {
   "FIELD1": "Haiti",
   "FIELD2": "HTI"
 },
 {
   "FIELD1": "Holy See",
   "FIELD2": "VAT"
 },
 {
   "FIELD1": "Honduras",
   "FIELD2": "HND"
 },
 {
   "FIELD1": "Hungary",
   "FIELD2": "HUN"
 },
 {
   "FIELD1": "Iceland",
   "FIELD2": "ISL"
 },
 {
   "FIELD1": "India",
   "FIELD2": "IND"
 },
 {
   "FIELD1": "Indonesia",
   "FIELD2": "IDN"
 },
 {
   "FIELD1": "Iran (Islamic Republic of)",
   "FIELD2": "IRN"
 },
 {
   "FIELD1": "Iraq",
   "FIELD2": "IRQ"
 },
 {
   "FIELD1": "Ireland",
   "FIELD2": "IRL"
 },
 {
   "FIELD1": "Israel",
   "FIELD2": "ISR"
 },
 {
   "FIELD1": "Italy",
   "FIELD2": "ITA"
 },
 {
   "FIELD1": "Jamaica",
   "FIELD2": "JAM"
 },
 {
   "FIELD1": "Japan",
   "FIELD2": "JPN"
 },
 {
   "FIELD1": "Jordan",
   "FIELD2": "JOR"
 },
 {
   "FIELD1": "Kazakhstan",
   "FIELD2": "KAZ"
 },
 {
   "FIELD1": "Kenya",
   "FIELD2": "KEN"
 },
 {
   "FIELD1": "Kiribati",
   "FIELD2": "KIR"
 },
 {
   "FIELD1": "Korea (Democratic People's Republic of)",
   "FIELD2": "PRK"
 },
 {
   "FIELD1": "Korea (Republic of)",
   "FIELD2": "KOR"
 },
 {
   "FIELD1": "Kuwait",
   "FIELD2": "KWT"
 },
 {
   "FIELD1": "Kyrgyzstan",
   "FIELD2": "KGZ"
 },
 {
   "FIELD1": "Lao People's Democratic Republic",
   "FIELD2": "LAO"
 },
 {
   "FIELD1": "Latvia",
   "FIELD2": "LVA"
 },
 {
   "FIELD1": "Lebanon",
   "FIELD2": "LBN"
 },
 {
   "FIELD1": "Lesotho",
   "FIELD2": "LSO"
 },
 {
   "FIELD1": "Liberia",
   "FIELD2": "LBR"
 },
 {
   "FIELD1": "Libya",
   "FIELD2": "LBY"
 },
 {
   "FIELD1": "Liechtenstein",
   "FIELD2": "LIE"
 },
 {
   "FIELD1": "Lithuania",
   "FIELD2": "LTU"
 },
 {
   "FIELD1": "Luxembourg",
   "FIELD2": "LUX"
 },
 {
   "FIELD1": "Macedonia",
   "FIELD2": "MKD"
 },
 {
   "FIELD1": "Madagascar",
   "FIELD2": "MDG"
 },
 {
   "FIELD1": "Malawi",
   "FIELD2": "MWI"
 },
 {
   "FIELD1": "Malaysia",
   "FIELD2": "MYS"
 },
 {
   "FIELD1": "Maldives",
   "FIELD2": "MDV"
 },
 {
   "FIELD1": "Mali",
   "FIELD2": "MLI"
 },
 {
   "FIELD1": "Malta",
   "FIELD2": "MLT"
 },
 {
   "FIELD1": "Marshall Islands",
   "FIELD2": "MHL"
 },
 {
   "FIELD1": "Mauritania",
   "FIELD2": "MRT"
 },
 {
   "FIELD1": "Mauritius",
   "FIELD2": "MUS"
 },
 {
   "FIELD1": "Mexico",
   "FIELD2": "MEX"
 },
 {
   "FIELD1": "Micronesia (Federated States of)",
   "FIELD2": "FSM"
 },
 {
   "FIELD1": "Moldova (Republic of)",
   "FIELD2": "MDA"
 },
 {
   "FIELD1": "Monaco",
   "FIELD2": "MCO"
 },
 {
   "FIELD1": "Mongolia",
   "FIELD2": "MNG"
 },
 {
   "FIELD1": "Montenegro",
   "FIELD2": "MNE"
 },
 {
   "FIELD1": "Morocco",
   "FIELD2": "MAR"
 },
 {
   "FIELD1": "Mozambique",
   "FIELD2": "MOZ"
 },
 {
   "FIELD1": "Myanmar",
   "FIELD2": "MMR"
 },
 {
   "FIELD1": "Namibia",
   "FIELD2": "NAM"
 },
 {
   "FIELD1": "Nauru",
   "FIELD2": "NRU"
 },
 {
   "FIELD1": "Nepal",
   "FIELD2": "NPL"
 },
 {
   "FIELD1": "Netherlands",
   "FIELD2": "NLD"
 },
 {
   "FIELD1": "New Zealand",
   "FIELD2": "NZL"
 },
 {
   "FIELD1": "Nicaragua",
   "FIELD2": "NIC"
 },
 {
   "FIELD1": "Niger",
   "FIELD2": "NER"
 },
 {
   "FIELD1": "Nigeria",
   "FIELD2": "NGA"
 },
 {
   "FIELD1": "Norway",
   "FIELD2": "NOR"
 },
 {
   "FIELD1": "Oman",
   "FIELD2": "OMN"
 },
 {
   "FIELD1": "Pakistan",
   "FIELD2": "PAK"
 },
 {
   "FIELD1": "Palau",
   "FIELD2": "PLW"
 },
 {
   "FIELD1": "Panama",
   "FIELD2": "PAN"
 },
 {
   "FIELD1": "Papua New Guinea",
   "FIELD2": "PNG"
 },
 {
   "FIELD1": "Paraguay",
   "FIELD2": "PRY"
 },
 {
   "FIELD1": "Peru",
   "FIELD2": "PER"
 },
 {
   "FIELD1": "Philippines",
   "FIELD2": "PHL"
 },
 {
   "FIELD1": "Poland",
   "FIELD2": "POL"
 },
 {
   "FIELD1": "Portugal",
   "FIELD2": "PRT"
 },
 {
   "FIELD1": "Qatar",
   "FIELD2": "QAT"
 },
 {
   "FIELD1": "Romania",
   "FIELD2": "ROU"
 },
 {
   "FIELD1": "Russian Federation",
   "FIELD2": "RUS"
 },
 {
   "FIELD1": "Rwanda",
   "FIELD2": "RWA"
 },
 {
   "FIELD1": "Saint Kitts and Nevis",
   "FIELD2": "KNA"
 },
 {
   "FIELD1": "Saint Lucia",
   "FIELD2": "LCA"
 },
 {
   "FIELD1": "Saint Vincent and the Grenadines",
   "FIELD2": "VCT"
 },
 {
   "FIELD1": "Samoa",
   "FIELD2": "WSM"
 },
 {
   "FIELD1": "San Marino",
   "FIELD2": "SMR"
 },
 {
   "FIELD1": "Sao Tome and Principe",
   "FIELD2": "STP"
 },
 {
   "FIELD1": "Saudi Arabia",
   "FIELD2": "SAU"
 },
 {
   "FIELD1": "Senegal",
   "FIELD2": "SEN"
 },
 {
   "FIELD1": "Serbia",
   "FIELD2": "SRB"
 },
 {
   "FIELD1": "Seychelles",
   "FIELD2": "SYC"
 },
 {
   "FIELD1": "Sierra Leone",
   "FIELD2": "SLE"
 },
 {
   "FIELD1": "Singapore",
   "FIELD2": "SGP"
 },
 {
   "FIELD1": "Slovakia",
   "FIELD2": "SVK"
 },
 {
   "FIELD1": "Solomon Islands",
   "FIELD2": "SLB"
 },
 {
   "FIELD1": "Somalia",
   "FIELD2": "SOM"
 },
 {
   "FIELD1": "South Africa",
   "FIELD2": "ZAF"
 },
 {
   "FIELD1": "South Sudan",
   "FIELD2": "SSD"
 },
 {
   "FIELD1": "Spain",
   "FIELD2": "ESP"
 },
 {
   "FIELD1": "Sri Lanka",
   "FIELD2": "LKA"
 },
 {
   "FIELD1": "Sudan",
   "FIELD2": "SDN"
 },
 {
   "FIELD1": "Suriname",
   "FIELD2": "SUR"
 },
 {
   "FIELD1": "Swaziland",
   "FIELD2": "SWZ"
 },
 {
   "FIELD1": "Sweden",
   "FIELD2": "SWE"
 },
 {
   "FIELD1": "Switzerland",
   "FIELD2": "CHE"
 },
 {
   "FIELD1": "Syrian Arab Republic",
   "FIELD2": "SYR"
 },
 {
   "FIELD1": "Tajikistan",
   "FIELD2": "TJK"
 },
 {
   "FIELD1": "Tanzania, United Republic of",
   "FIELD2": "TZA"
 },
 {
   "FIELD1": "Thailand",
   "FIELD2": "THA"
 },
 {
   "FIELD1": "Timor-Leste",
   "FIELD2": "TLS"
 },
 {
   "FIELD1": "Togo",
   "FIELD2": "TGO"
 },
 {
   "FIELD1": "Tonga",
   "FIELD2": "TON"
 },
 {
   "FIELD1": "Trinidad and Tobago",
   "FIELD2": "TTO"
 },
 {
   "FIELD1": "Tunisia",
   "FIELD2": "TUN"
 },
 {
   "FIELD1": "Turkey",
   "FIELD2": "TUR"
 },
 {
   "FIELD1": "Turkmenistan",
   "FIELD2": "TKM"
 },
 {
   "FIELD1": "Tuvalu",
   "FIELD2": "TUV"
 },
 {
   "FIELD1": "Uganda",
   "FIELD2": "UGA"
 },
 {
   "FIELD1": "Ukraine",
   "FIELD2": "UKR"
 },
 {
   "FIELD1": "United Arab Emirates",
   "FIELD2": "ARE"
 },
 {
   "FIELD1": "United Kingdom of Great Britain and Northern Ireland",
   "FIELD2": "GBR"
 },
 {
   "FIELD1": "United States of America",
   "FIELD2": "USA"
 },
 {
   "FIELD1": "Uruguay",
   "FIELD2": "URY"
 },
 {
   "FIELD1": "Uzbekistan",
   "FIELD2": "UZB"
 },
 {
   "FIELD1": "Vanuatu",
   "FIELD2": "VUT"
 },
 {
   "FIELD1": "Venezuela (Bolivarian Republic of)",
   "FIELD2": "VEN"
 },
 {
   "FIELD1": "Viet Nam",
   "FIELD2": "VNM"
 },
 {
   "FIELD1": "Yemen",
   "FIELD2": "YEM"
 },
 {
   "FIELD1": "Zambia",
   "FIELD2": "ZMB"
 },
 {
   "FIELD1": "Zimbabwe",
   "FIELD2": "ZWE"
 }
];

function generiraj() {
    $("#generiranjePacientovObvestilo").html("<div id='pacienti' class='alert alert-success alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>Uspešno generirani podatki: </div>");
    for (var i = 1; i < 4; i++) {
        generirajPodatke(i);
        //console.log("To je id" + taEhrId);
        //$("#generiranjePacientovObvestilo").append("<div>" + pacientiVzorec[i-1][0] + " " + pacientiVzorec[i-1][1] + ": " + generirajPodatke(i) + "</div>");
    }
}
 
function generirajPodatke(stPacienta) {
    var ehrId = "";
  
    var index = stPacienta - 1;
    $("#imeInfo").val = pacientiVzorec[index][0];
    $("#priimekInfo").val = pacientiVzorec[index][1];
    $("#datumInfo").val = pacientiVzorec[index][2];
    $("#sladkornaInfo").val = pacientiVzorec[index][3];
    
    dodajEHR(index, function(id) {
        ehrId = id;
        meritveVzorec[index][0] = id;
        //console.log(id);
        $("#pacienti").append("<div>" + pacientiVzorec[index][0] + " " + pacientiVzorec[index][1] + ": " + id + "</div>");
    });
        //dodajMeritve(id, meritvePacientov[index][1], meritvePacientov[index][2], meritvePacientov[index][3], meritvePacientov[index][4], meritvePacientov[index][5], meritvePacientov[index][6], function() {
            //alert("Dodana meritev!");
            //});
    return ehrId;
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
//var ehrId;
//Kreiranje EHR novega pacienta
function dodajEHR(indeks, callback) {
	sessionId = getSessionId();
	
	var ime = "";
	var priimek = "";
	var datumRojstva = "";
	var sladkorna = "";
	
	if (indeks == -1) {
	    ime = $("#imeInfo").val();
	    priimek = $("#priimekInfo").val();
	    datumRojstva = $("#datumInfo").val();
	    sladkorna = $("#sladkornaInfo").val();
	}
	else {
	    ime = pacientiVzorec[indeks][0];
	    priimek = pacientiVzorec[indeks][1];
	    datumRojstva = pacientiVzorec[indeks][2];
	    sladkorna = pacientiVzorec[indeks][3];
	}


	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreiranjeSporocilo").html("<div class='alert alert-warning alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>Preverite in izpolnite manjkajoče podatke!</div>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            "firstNames": ime,
		            "lastNames": priimek,
		            "dateOfBirth": datumRojstva,
		            "partyAdditionalInfo": [{"key": "ehrId", "value": ehrId}, {"key": "sladkorna", "value": sladkorna}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    if (indeks == -1) {
		                        $("#kreiranjeSporocilo").html("<div class='alert alert-success alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>Pacient je bil uspešno dodan. EHR: " + ehrId + "</div>");
		                    }
		                    //$("#preberiEHRid").val(ehrId);
		                    if (typeof callback === "function") {
		                        callback(ehrId);
		                    }
		                    //console.log(ehrId);
		                }
		            },
		            error: function(err) {
		                if (indeks == -1) {
		                   $("#kreiranjeSporocilo").html("<div class='alert alert-danger alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>Napaka: " +
                        JSON.parse(err.responseText).userMessage + "'!'</div>");
		                }
		            }
		        });
		    }
		});
	}
}
var prisotni = 1;

//Dodajanje meritev
function dodajMeritveVitalnihZnakov(EHRID, datumUra, visina, teza, callback) {
    sessionId = getSessionId();

    if (!EHRID || !datumUra || !visina || !teza) {
        $("#meritveSporocilo").html("<div class='alert alert-warning alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>Preverite in izpolnite manjkajoče podatke!</div>");
    }
    else {

        $.ajaxSetup({
            headers: {
                "Ehr-Session": sessionId
            }
        });

        var podatki = {
            // Struktura predloge je na voljo na naslednjem spletnem naslovu:
            // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
            "ctx/language": "en",
            "ctx/territory": "SI",
            "ctx/time": datumUra,
            "vital_signs/body_weight/any_event/body_weight": teza,
            "vital_signs/height_length/any_event/body_height_length": visina,
            "vital_signs/body_temperature/any_event/temperature|unit": "°C"
        };

        var parametriZahteve = {
            ehrId: EHRID,
            templateId: 'Vital Signs',
            format: 'FLAT',
        };

        $.ajax({
            url: baseUrl + "/composition?" + $.param(parametriZahteve),
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(podatki),
            success: function(res) {
                prisotni = 1;
                $("#meritveSporocilo").html("<div class='alert alert-success alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>Meritev je bila uspešno dodana: " + res.meta.href + "</div>");
                if (typeof callback === "function") {
		           callback();
		        }
            },
            error: function(err) {
                $("#meritveSporocilo").html("<div class='alert alert-danger' role='alert'>Napaka: " + JSON.parse(err.responseText).userMessage + "!</div>");
            }
        });
    }
}



function analizirajIndeks() {

    var bereit = 0;

    sessionId = getSessionId();

    var EHRID = $("#ehrIdIndeks").val();

    if (!EHRID) {
        $("#IzracunObvestilo").html("<div class='alert alert-danger alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>Prosimo vnesite EHR ID pacienta.</div>");
        prisotni = -1;
    }
    else {

        $.ajax({
            url: baseUrl + "/demographics/ehr/" + EHRID + "/party",
            type: 'GET',
            headers: {
                "Ehr-Session": sessionId
            },

            success: function(data) {

                var party = data.party;

                //nastavi demografske podatke za pacienta
                imeIndeks = party.firstNames;
                priimekIndeks = party.lastNames;
                datumInUraIndeks = party.dateOfBirth;
                dodatneInformacije = party.partyAdditionalInfo;
                //console.log("partyJSON: " + partyAdditional);

                //TEŽA
                $.ajax({
                    url: baseUrl + "/view/" + EHRID + "/" + "weight",
                    type: 'GET',
                    headers: {
                        "Ehr-Session": sessionId
                    },
                    success: function(res) {
                        bereit++;
                        if (res.length > 0) {
                            tezaIndeks = res[0].weight;
                            datumIndeks = res[0].time;
                        }
                        else {
                            prisotni = -1;

                        }
                        if (bereit === 2) {
                            analizirajPodatke();
                        }

                    },
                    error: function() {
                        alert(JSON.parse(err.responseText).userMessage);
                    }
                });

                //VIŠINA
                $.ajax({
                    url: baseUrl + "/view/" + EHRID + "/" + "height",
                    type: 'GET',
                    headers: {
                        "Ehr-Session": sessionId
                    },
                    success: function(res) {
                        bereit++;
                        if (res.length > 0) {
                            visinaIndeks = res[0].height;
                        }
                        else {
                            prisotni = -1;

                        }
                        if (bereit === 2) {
                            analizirajPodatke();
                        }

                    },
                    error: function() {
                        alert(JSON.parse(err.responseText).userMessage);
                    }
                });

            },
            error: function(err) {
                alert(JSON.parse(err.responseText).userMessage);
            }
        });

    }

}


function analizirajPodatke() {

    var sladkornaIndeks = 0;
    for (var i = 0; i < dodatneInformacije.length; i++) {
        if (dodatneInformacije[i]["key"] === "sladkorna") {
            sladkornaIndeks = dodatneInformacije[i]["value"];
        }
    }
    

    if (prisotni === 1) {
        //NASTAVI PODATKE
        $("#imeIndeks").html("<b>Ime: </b>" + imeIndeks + " " + priimekIndeks);
        $("#starostIndeks").html("<b>Rojstni datum: </b>" + datumInUraIndeks);
        $("#datumMeritveIndeks").html("<b>Datum meritve: </b>" + datumIndeks);
        $("#tezaIndeks").html("<b>Telesna teža: </b>" + tezaIndeks + "kg");
        $("#visinaIndeks").html("<b>Telesna višina: </b>" + visinaIndeks + "cm");
        var itmIndeks = 0;


        //IZRAČUNAJ ITM
        var itm = (tezaIndeks) / ((visinaIndeks / 100) * (visinaIndeks / 100));
        itm = Math.round(itm * 100) / 100;
        
        if (itm < 18.5) {
            itmIndeks = -1;
            $("#sporociloItm").html("<span class='obvestilo label label-warning fade-in' data-toggle='tooltip' data-placement='top' title='Idealna vrednost: 18.5 - 24.9'>" + itm + ", oseba je presuha" + ".</span>");
        }
        else if ((itm >= 18.5) && (itm <= 24.9)) {
            itmIndeks = 0;
            $("#sporociloItm").html("<span class='obvestilo label label-success fade-in' data-toggle='tooltip' data-placement='top' title='Idealna vrednost: 18.5 - 24.9'>" + itm + ", oseba ima normalno telesno maso" + ".</span>");
        }
        else if ((itm > 24.9) && (itm <= 29.9)) {
            itmIndeks = 1;
            $("#sporociloItm").html("<span class='obvestilo label label-warning fade-in' data-toggle='tooltip' data-placement='top' title='Idealna vrednost: 18.5 - 24.9'>" + itm + ", oseba ima zvečano telesno maso" + ".</span>");
        }
        else if ((itm >= 30.0) && (itm <= 34.9)) {
            itmIndeks = 2;
            $("#sporociloItm").html("<span class='obvestilo label label-danger fade-in' data-toggle='tooltip' data-placement='top' title='Idealna vrednost: 18.5 - 24.9'>" + itm + ", oseba je predebela po stopnji I" + ".</span>");
        }
        else if ((itm > 34.9) && (itm <= 39.9)) {
            itmIndeks = 3;
            $("#sporociloItm").html("<span class='obvestilo label label-danger fade-in' data-toggle='tooltip' data-placement='top' title='Idealna vrednost: 18.5 - 24.9'>" + itm + ", oseba je predebela po stopnji II" + ".</span>");
        }
        else if (itm > 39.9) {
            itmIndeks = 4;
            $("#sporociloItm").html("<span class='obvestilo label label-danger fade-in' data-toggle='tooltip' data-placement='top' title='Idealna vrednost: 18.5 - 24.9'>" + itm + ", oseba je predebela po stopnji III" + ".</span>");
        }
        
        
        //PRIPOROČILA
        var priporociloItm = "";
        var priporociloSladkorna = "";
        var alertOkno = 0;
        var alertOkno2 = 0;
        var alertOknoRezultat = "";
        
        if (sladkornaIndeks == 1) {
            priporociloSladkorna = " Glede na to, da imate v sorodstvu osebe s sladkorno boleznijo, morate paziti, da jeste zdravo in uravnoteženo prehrano.";
            alertOkno = 1;
        }
        else if (sladkornaIndeks == 2) {
            priporociloSladkorna = " Ker imate sladkorno bolezen, morate še posebno paziti s kakšno hrano se prehranjujete. Obrnite se na vašega osebnega zdravnika.";
            alertOkno = 2;
        }
        else {
            priporociloSladkorna = "";
            alertOkno = 0;
        }
        
        switch (itmIndeks) {
            case -1:
                priporociloItm = "Ste podhranjeni in morate jesti več.";
                alertOkno2 = 1;
                break;
            case 0:
                priporociloItm = "Vaša telesna teža je ravno pravšnja.";
                alertOkno2 = 0;
                break;
            case 1:
                priporociloItm = "Vaša telesna teža je rahlo previsoka. Za izboljšanje vašega stanja se več ukvarjajte s športom in rekreacijo.";
                alertOkno2 = 1;
                break;
            case 2:
                priporociloItm = "Vaša telesna teža je previsoka. Za izboljšanje vašega stanja se več ukvarjajte s športom in rekreacijo, iz vašega jedilnika pa izločite sladka in mastna živila.";
                alertOkno2 = 2;
                break;
            case 3:
                priporociloItm = "Vaša telesna teža je močno previsoka, kar ogroža vaše zdravstveno stanje. Za izboljšanje vašega stanja se morate redno ukvarjajti s športom in rekreacijo, iz vašega jedilnika pa morate izločiti sladka in mastna živila.";
                alertOkno2 = 2;
                break;
            case 4:
                priporociloItm = "Ste močno predebeli in vaše zdravstveno stanje je ogroženo. Za izboljšanje vašega stanja se morate redno ukvarjati s športom in rekreacijo, iz vašega jedilnika pa morate izločiti večino sladkih, mastnih in škrobnatih živil.";
                alertOkno2 = 2;
                break;
        }
        
        if (alertOkno2 > alertOkno) {
            alertOkno = alertOkno2;
        }
        if (alertOkno == 0) {
            alertOknoRezultat = "success";
        }
        else if (alertOkno == 1) {
            alertOknoRezultat = "warning";
        }
        else {
            alertOknoRezultat = "danger";
        }
        
        $("#priporocila").html("<div id='sporocilo' class='alert alert-" + alertOknoRezultat + " alert-dismissible' role='alert'><button type='button' color='#d9534f' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" + priporociloItm + priporociloSladkorna + "</div>");
        $("#IzracunObvestilo").html("");
        
        var priporocenaTezaMin = (visinaIndeks / 100) * (visinaIndeks / 100) * 18.5;
        priporocenaTezaMin = Math.round(priporocenaTezaMin * 100) / 100;
        var priporocenaTezaMax = (visinaIndeks / 100) * (visinaIndeks / 100) * 24.9;
        priporocenaTezaMax = Math.round(priporocenaTezaMax * 100) / 100;

        $("#idealnaTezaIndeks").html("<b>Vaša idealna telesna teža je med: </b>" + priporocenaTezaMin + " in " + priporocenaTezaMax + " kg.");
        if ((itm >= 18.5) && (itm <= 24.9)) {
            $("#idealnaTezaIndeks").css("color", "#5cb85c");
        }
        else {
            $("#idealnaTezaIndeks").css("color", "#d9534f");
        }
        //$("#idealnaTezaIndeks").css("display", "none");
        
    }
    else {
        $("#IzracunObvestilo").html("<div class='alert alert-warning alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>Ni podatkov. Prosimo, vnesite meritve.</div>");
        console.log("No EHRID data");
    }

}

var drz = "SVN";
var tabelaM = [];
var tabelaZ = [];
var grafDrzav;

function pridobiPodatkeWHO() {
            $.ajax({
                    type: "GET",
                    url: "https://cors-anywhere.herokuapp.com/http://apps.who.int/gho/athena/api/GHO/NCD_BMI_MEANC.json?profile=simple&filter=COUNTRY:" + drz,
                    contentType: "application/json; charset=utf-8",
                    async: false,
                    dataType: "json",
                    success: function (data) {
                        var tabela = data.fact;
                        var stElementov = tabela.length;
                        for (var i = 1950; i < 2017; i++) {
	                        for (var j = 0; j < tabela.length; j++) {
		                    if (tabela[j].dim.YEAR == i) {
		                        var itm  = parseFloat(tabela[j].Value);
		                        if (tabela[j].dim.SEX == "Male") {
		                            var objM = {x: i, y: itm};
		                            tabelaM.push(objM);
		                        }
		                        else {
		                            var objZ = {x: i, y: itm};
		                            tabelaZ.push(objZ);
		                        }
		                        //console.log(i + " " + itm);
		                    }
	                        }
                        }
                        //console.log(tabelaM);
                        //console.log(tabelaZ);
                        //console.log(data);

        },
        error: function (errorMessage) {
        }
    });
}

function dodajGraf() {
                            //GRAF
                        
        grafDrzav = new Rickshaw.Graph({
                        element: document.querySelector("#chart"),
                        renderer: 'line',
                        min: 18,
                        max: 32,
                        stack: false,
                        series: [{
                            color: 'lightgreen',
                            data: tabelaM,
                            name: 'Moški'
                            }, {
                            color: 'lightblue',
                            data: tabelaZ,
                            name: 'Ženske'
                            }]
                        });
                        
                        //X in Y OS
                        
                        var xAxis = new Rickshaw.Graph.Axis.X({
                            graph: grafDrzav
                        });
                        xAxis.render();
                        
                        var yAxis = new Rickshaw.Graph.Axis.Y({
                        graph: grafDrzav
                        });
                        yAxis.render();
                        
                        grafDrzav.render();
                        
                        /*var hoverDetail = new Rickshaw.Graph.HoverDetail( {
                        graph: graph,
                        xFormatter: function(x) { return "Leto: " + x },
                        yFormatter: function(y) { return "ITM: " + y }
                        } );
                        */
                        
                        //LEGENDA IN HOVER
                        
                    var legend = document.querySelector('#legend');
                    
                    var Hover = Rickshaw.Class.create(Rickshaw.Graph.HoverDetail, {

	                render: function(args) {

		            legend.innerHTML = args.formattedXValue;

		            args.detail.sort(function(a, b) { return a.order - b.order }).forEach( function(d) {

			        var line = document.createElement('div');
			        line.className = 'line';

			        var swatch = document.createElement('div');
			        swatch.className = 'swatch';
			        swatch.style.backgroundColor = d.series.color;

			        var label = document.createElement('div');
			        //console.log(label);
			        label.className = 'label" "color="black';
			        label.innerHTML = d.name + ": " + d.formattedYValue;

			        line.appendChild(swatch);
			        line.appendChild(label);

			        legend.appendChild(line);

			        var dot = document.createElement('div');
			        dot.className = 'dot';
			        dot.style.top = grafDrzav.y(d.value.y0 + d.value.y) + 'px';
			        dot.style.borderColor = d.series.color;

			        this.element.appendChild(dot);

			        dot.className = 'dot active';
			        

			        this.show();

		              }, this );
                    }
                });

                var hover = new Hover( { graph: grafDrzav } );
 
                    //var markup = data.parse.text["*"];
                    //var blurb = $('<div></div>').html(markup);
                    //$('#videoPrispevek').html($(blurb).find('p'));
                    //$("#vir").html('<h5>Vir: https://sl.wikipedia.org/wiki/Debelost</h5>');
                
}

$(document).ready(function() {
    
    $("#ehrIdIndeks").change(function() {
       prisotni = 1; 
    });
    
    $("#izbiraPacienta").change(function() {
        var index = $("#izbiraPacienta").val();

        $("#imeInfo").val(pacientiVzorec[index][0]);
        $("#priimekInfo").val(pacientiVzorec[index][1]);
        $("#datumInfo").val(pacientiVzorec[index][2]);
        $("#sladkornaInfo").val(pacientiVzorec[index][3]);
    });
    
    $("#izbiraPacientaMeritve").change(function() {
        var index = $("#izbiraPacientaMeritve").val();

        $("#ehrIdMeritev").val(meritveVzorec[index][0]);
        $("#datumMeritev").val(meritveVzorec[index][1]);
        $("#visinaMeritev").val(meritveVzorec[index][2]);
        $("#tezaMeritev").val(meritveVzorec[index][3]);
    });
    
    $("#izbiraDodatnega").change(function() {
        var index = $("#izbiraDodatnega").val();
        
        if (index == 0) {
            $('#chart').html("");
            $('#izbiraDrzav').html('');
            $('#legend').html('');
            //console.log(index);
            $("#videoPrispevek").html('<h4>Video - Statistični podatki o debelosti</h4></br><iframe id="youtubeVid" width="410" height="220" src="https://www.youtube.com/embed/IkoC3HZwe7Q" frameborder="0" allowfullscreen></iframe>');
            $("#vir").html('<h5>Vir: https://www.youtube.com/watch?v=IkoC3HZwe7Q</h5>');
        }
        else if (index == 1) {
            $('#chart').html("");
            $('#izbiraDrzav').html('');
            $('#legend').html('');
                $.ajax({
                    type: "GET",
                    url: "https://sl.wikipedia.org/w/api.php?action=parse&format=json&prop=text&section=0&page=Debelost&callback=?",
                    contentType: "application/json; charset=utf-8",
                    async: false,
                    dataType: "json",
                    success: function (data, textStatus, jqXHR) {
 
                    var markup = data.parse.text["*"];
                    var blurb = $('<div></div>').html(markup);
                    $('#videoPrispevek').html($(blurb).find('p'));
                    $("#vir").html('<h5>Vir: https://sl.wikipedia.org/wiki/Debelost</h5>');
 
        },
        error: function (errorMessage) {
        }
                });
        }
        
        else if (index == 2) {
            drz = 'SVN';
            tabelaM.length = 0;
            tabelaZ.length = 0;
            pridobiPodatkeWHO();
            dodajGraf();
                    $('#videoPrispevek').html('');
                    $('#izbiraDrzav').html('');
                    $('#legend').html('');
                     $('#vir').html('Vir: WHO. Povprečni indeks telesne mase.');
            var izbiraDrzav = document.querySelector("#izbiraDrzav");
                var drzaveString = "";
                for (var k = 0; k < drzave.length; k++) {
                    drzaveString = drzaveString + "<option value=" + k + ">" + drzave[k].FIELD1 + "</option>";
                }
                izbiraDrzav.innerHTML = '<select id="izbiraDrzave" class="form-control">' + drzaveString + '</select>';
                
            $("#izbiraDrzave").change(function() {
            var drzIndeks = $("#izbiraDrzave").val();
            drz = drzave[drzIndeks].FIELD2;
            tabelaM.length = 0;
            tabelaZ.length = 0;
            //console.log(drz);
            pridobiPodatkeWHO();
            $('#chart').html("");
            dodajGraf();
    });
 
        }
    });
        
    $("#izbiraDrzave").change(function() {
        var drzIndeks = $("#izbiraDrzave").val();
            drz = drzave[drzIndeks].FIELD2;
            tabelaM.length = 0;
            tabelaZ.length = 0;
            console.log(drz);
            pridobiPodatkeWHO();
            $('#chart').html("");
            dodajGraf();
    });
    
    $("#izbiraPacientaIndeks").change(function() {
        var index = $("#izbiraPacientaIndeks").val();

        $("#ehrIdIndeks").val(meritveVzorec[index][0]);
    });
    
    
    $("#dodajMeritev").click(function() {
        var EHRID = $("#ehrIdMeritev").val();
        var datumUra = $("#datumMeritev").val();
        var visina = $("#visinaMeritev").val();
        var teza = $("#tezaMeritev").val();

        dodajMeritveVitalnihZnakov(EHRID, datumUra, visina, teza);
    });
});
